import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios'

export default function Example(){
    const[regiones, setRegiones] = useState([]);
    const[provincias, setProvincias] = useState([]);
    const[ciudades, setCiudades] = useState([]);
    const[calles, setCalles] = useState([]);
    const[filtrarprovincias, setFiltrarprovincias] = useState([]);
    const[filtrarciudades, setFiltrarciudades] = useState([]);
    const[filtrarcalles, setFiltrarcalles] = useState([]);

    const obtenerRegiones = () => {
        axios.get('http://127.0.0.1:8000/api/regiones')
        .then(response => {
            setRegiones(response.data)
        }).catch(error => {
            console.log(error)
        })
    }

    const obtenerProvincias = () => {
        axios.get('http://127.0.0.1:8000/api/provincias')
        .then(response => {
            setProvincias(response.data)
        }).catch( error => {
            console.log(error)
        })
    }

    const handlerCargarProvincias = function(e){
        const opcion = e.target.value;
        console.log(opcion)
        
        let data = provincias.filter(provincias => provincias.id_region === Number(opcion))
        console.log(data)
        setFiltrarprovincias(data)
        return data
    }

    const obtenerCiudades = () => {
        axios.get('http://127.0.0.1:8000/api/ciudades')
        .then(response => {
            setCiudades(response.data)
        }).catch( error => {
            console.log(error)
        })
    }

    const handlerCargarCiudades = function(e){
        const opcion = e.target.value;
        let data = ciudades.filter(ciudades => ciudades.id_provincia === Number(opcion))
        console.log(data)
        setFiltrarciudades(data)
        return data
    }

    const obtenerCalles = () =>{
        axios.get('http://127.0.0.1:8000/api/calles')
        .then(response => {
            setCalles(response.data)
        }).catch( error => {
            console.log(error)
        })
    }

    const handlerCargarCalles = function(e){
        const opcion = e.target.value;
        let data = calles.filter(calles => calles.id_ciudad === Number(opcion))
        console.log(data)
        setFiltrarcalles(data)
        return data

    }

    useEffect(obtenerRegiones, []);
    useEffect(obtenerProvincias, []);
    useEffect(obtenerCiudades, []);
    useEffect(obtenerCalles, []);

    return(
        <div className="container">
            <div className="col-md-8 justify-content-center">
                    <label>Region:</label>
                    <select className="form-control mb-3" onChange={handlerCargarProvincias}>
                    <option value="-1">Seleccione region</option>
                    {
                        regiones.map(elemento => (
                            <option key={elemento.id_region} value={elemento.id_region}>{elemento.nombre_region}</option>
                        ))
                    }
                    </select>
                    <label>Provincia:</label>
                    <select className="form-control mb-3" onChange={handlerCargarCiudades}>
                        <option value="-1">Seleccione provincia</option>
                        {
                            filtrarprovincias.map(elemento => (
                                <option key={elemento.id_provincia} value={elemento.id_provincia}>{elemento.nombre_provincia}</option>
                            ))
                        }
                    </select>
                    <label>Ciudad:</label>
                    <select className="form-control mb-3" onChange={handlerCargarCalles}>
                        <option value="-1">Seleccione ciudad</option>
                        {
                            filtrarciudades.map(elemento => (
                                <option key={elemento.id_ciudad} value={elemento.id_ciudad}>{elemento.nombre_ciudad}</option>
                            ))
                        }
                    </select>
                    <div className="row">
                        <div className="col-8 mx-auto border border-dark">
                            <p className="text-center">Calles:</p>
                            {
                                filtrarcalles.map(elemento => (
                                <div className="text-center">
                                <li key={elemento.id_calle}>{elemento.nombre_calle}</li>
                                </div>
                                ))
                            }
                        </div>
                    </div>    
            </div>
        </div>
    )
}

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}

