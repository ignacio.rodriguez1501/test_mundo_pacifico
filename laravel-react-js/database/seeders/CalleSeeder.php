<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calles')->insert([
            [
                'nombre_calle' => 'Progreso',
                'id_ciudad' => '1'
            ],
            [
                'nombre_calle' => 'Independencia',
                'id_ciudad' => '1'
            ],
            [
                'nombre_calle' => 'Manquimavida',
                'id_ciudad' => '1'
            ],
            [
                'nombre_calle' => 'Freire',
                'id_ciudad' => '2'
            ],
            [
                'nombre_calle' => 'Maipu',
                'id_ciudad' => '2'
            ],
            [
                'nombre_calle' => 'Los Carrera',
                'id_ciudad' => '2'
            ],
            [
                'nombre_calle' => 'Los Chiflones',
                'id_ciudad' => '3'
            ],
            [
                'nombre_calle' => 'Minero Manuel Rivas',
                'id_ciudad' => '3'
            ],
            [
                'nombre_calle' => 'Las Toscas',
                'id_ciudad' => '3'
            ],
            [
                'nombre_calle' => 'Dover',
                'id_ciudad' => '4'
            ],
            [
                'nombre_calle' => 'Grecia',
                'id_ciudad' => '4'
            ],
            [
                'nombre_calle' => 'Bristol',
                'id_ciudad' => '4'
            ],
            [
                'nombre_calle' => 'Michaihue',
                'id_ciudad' => '5'
            ],
            [
                'nombre_calle' => 'Las Palmeras',
                'id_ciudad' => '5'
            ],
            [
                'nombre_calle' => 'Los Naranjos',
                'id_ciudad' => '5'
            ],
            [
                'nombre_calle' => 'Alto Horno',
                'id_ciudad' => '6'
            ],
            [
                'nombre_calle' => 'El Nogal',
                'id_ciudad' => '6'
            ],
            [
                'nombre_calle' => 'Los Olmos',
                'id_ciudad' => '6'
            ],
            [
                'nombre_calle' => 'Uribe',
                'id_ciudad' => '7'
            ],
            [
                'nombre_calle' => 'Tucapel',
                'id_ciudad' => '7'
            ],
            [
                'nombre_calle' => 'Los Mañios',
                'id_ciudad' => '7'
            ],
            [
                'nombre_calle' => 'Fresia',
                'id_ciudad' => '8'
            ],
            [
                'nombre_calle' => 'Las Araucarias',
                'id_ciudad' => '8'
            ],
            [
                'nombre_calle' => 'Los Tilos',
                'id_ciudad' => '8'
            ],
            [
                'nombre_calle' => '21 de Mayo',
                'id_ciudad' => '9'
            ],
            [
                'nombre_calle' => 'Abal',
                'id_ciudad' => '9'
            ],
            [
                'nombre_calle' => 'Alameda',
                'id_ciudad' => '9'
            ],
            [
                'nombre_calle' => 'Lillo',
                'id_ciudad' => '10'
            ],
            [
                'nombre_calle' => 'Guacolda',
                'id_ciudad' => '10'
            ],
            [
                'nombre_calle' => 'Mackay',
                'id_ciudad' => '10'
            ],
            [
                'nombre_calle' => 'La Cruz',
                'id_ciudad' => '11'
            ],
            [
                'nombre_calle' => 'Los Alamos',
                'id_ciudad' => '11'
            ],
            [
                'nombre_calle' => 'Las Rosas',
                'id_ciudad' => '11'
            ],
            [
                'nombre_calle' => 'Avenida Matta',
                'id_ciudad' => '12'
            ],
            [
                'nombre_calle' => 'Arriagada',
                'id_ciudad' => '12'
            ],
            [
                'nombre_calle' => 'Anibal Pinto',
                'id_ciudad' => '12'
            ],
            [
                'nombre_calle' => 'Pradenas',
                'id_ciudad' => '13'
            ],
            [
                'nombre_calle' => 'El Morro',
                'id_ciudad' => '13'
            ],
            [
                'nombre_calle' => 'Ejercito',
                'id_ciudad' => '13'
            ],
            [
                'nombre_calle' => 'Vallejos',
                'id_ciudad' => '14'
            ],
            [
                'nombre_calle' => 'Libertad',
                'id_ciudad' => '14'
            ],
            [
                'nombre_calle' => 'Baquedano',
                'id_ciudad' => '14'
            ],
            [
                'nombre_calle' => 'Covadonga',
                'id_ciudad' => '15'
            ],
            [
                'nombre_calle' => 'Lautaro',
                'id_ciudad' => '15'
            ],
            [
                'nombre_calle' => 'Bulnes',
                'id_ciudad' => '15'
            ],
            [
                'nombre_calle' => 'La Canada',
                'id_ciudad' => '16'
            ],
            [
                'nombre_calle' => 'Manantiales',
                'id_ciudad' => '16'
            ],
            [
                'nombre_calle' => 'Nueve',
                'id_ciudad' => '16'
            ],
            [
                'nombre_calle' => 'Barros Luco',
                'id_ciudad' => '17'
            ],
            [
                'nombre_calle' => 'Herrera',
                'id_ciudad' => '17'
            ],
            [
                'nombre_calle' => 'Balmaceda',
                'id_ciudad' => '17'
            ],
            [
                'nombre_calle' => 'El Espino',
                'id_ciudad' => '18'
            ],
            [
                'nombre_calle' => 'El Roble',
                'id_ciudad' => '18'
            ],
            [
                'nombre_calle' => 'Daniel Moya',
                'id_ciudad' => '18'
            ],
            [
                'nombre_calle' => 'Domingo Tocornal',
                'id_ciudad' => '19'
            ],
            [
                'nombre_calle' => 'Eduardo Cordero',
                'id_ciudad' => '19'
            ],
            [
                'nombre_calle' => 'Cantabria',
                'id_ciudad' => '19'
            ],
            [
                'nombre_calle' => 'Uno Sur',
                'id_ciudad' => '20'
            ],
            [
                'nombre_calle' => 'Dos Sur',
                'id_ciudad' => '20'
            ],
            [
                'nombre_calle' => 'Tres Sur',
                'id_ciudad' => '20'
            ],
            [
                'nombre_calle' => 'Las Bandurrias',
                'id_ciudad' => '21'
            ],
            [
                'nombre_calle' => 'El Cerrillo',
                'id_ciudad' => '21'
            ],
            [
                'nombre_calle' => 'La Lecheria',
                'id_ciudad' => '21'
            ],
            [
                'nombre_calle' => 'Holanda',
                'id_ciudad' => '22'
            ],
            [
                'nombre_calle' => 'Ramon Freire',
                'id_ciudad' => '22'
            ],
            [
                'nombre_calle' => 'Nordico',
                'id_ciudad' => '22'
            ],
            [
                'nombre_calle' => '1 Norte',
                'id_ciudad' => '23'
            ],
            [
                'nombre_calle' => '1 Oriente',
                'id_ciudad' => '23'
            ],
            [
                'nombre_calle' => '10 Oriente',
                'id_ciudad' => '23'
            ],
            [
                'nombre_calle' => 'Avenida Calera de Tango',
                'id_ciudad' => '24'
            ],
            [
                'nombre_calle' => 'Pablo Neruda',
                'id_ciudad' => '24'
            ],
            [
                'nombre_calle' => 'Los Picunches',
                'id_ciudad' => '24'
            ],
            [
                'nombre_calle' => 'La Colonia',
                'id_ciudad' => '25'
            ],
            [
                'nombre_calle' => 'Huracan',
                'id_ciudad' => '25'
            ],
            [
                'nombre_calle' => 'Braun',
                'id_ciudad' => '25'
            ],
            [
                'nombre_calle' => 'Agua Potable',
                'id_ciudad' => '26'
            ],
            [
                'nombre_calle' => 'Antequiles',
                'id_ciudad' => '26'
            ],
            [
                'nombre_calle' => 'Avenida La Aurora',
                'id_ciudad' => '26'
            ],
            [
                'nombre_calle' => 'La Aguada',
                'id_ciudad' => '27'
            ],
            [
                'nombre_calle' => 'Porvenir',
                'id_ciudad' => '27'
            ],
            [
                'nombre_calle' => 'Esmeralda',
                'id_ciudad' => '27'
            ],
            [
                'nombre_calle' => 'Italia',
                'id_ciudad' => '28'
            ],
            [
                'nombre_calle' => 'Letras',
                'id_ciudad' => '28'
            ],
            [
                'nombre_calle' => 'Lima',
                'id_ciudad' => '28'
            ],
            [
                'nombre_calle' => 'Maria Isabel',
                'id_ciudad' => '29'
            ],
            [
                'nombre_calle' => 'Maria Vial',
                'id_ciudad' => '29'
            ],
            [
                'nombre_calle' => 'Brasil',
                'id_ciudad' => '29'
            ],
            [
                'nombre_calle' => 'Arcadia',
                'id_ciudad' => '30'
            ],
            [
                'nombre_calle' => 'Arcangel',
                'id_ciudad' => '30'
            ],
            [
                'nombre_calle' => 'Arquimides',
                'id_ciudad' => '30'
            ],
            [
                'nombre_calle' => 'Amanecer',
                'id_ciudad' => '31'
            ],
            [
                'nombre_calle' => 'Amapolas',
                'id_ciudad' => '31'
            ],
            [
                'nombre_calle' => 'Las Lilas',
                'id_ciudad' => '31'
            ],
            [
                'nombre_calle' => 'Los Alerces',
                'id_ciudad' => '32'
            ],[
                'nombre_calle' => 'Los Abeludes',
                'id_ciudad' => '32'
            ],[
                'nombre_calle' => 'Los Almendros',
                'id_ciudad' => '32'
            ],
            [
                'nombre_calle' => 'Juana Canales',
                'id_ciudad' => '33'
            ],
            [
                'nombre_calle' => 'Senador Jaime Guzman',
                'id_ciudad' => '33'
            ],
            [
                'nombre_calle' => 'Melipilla',
                'id_ciudad' => '33'
            ],
            [
                'nombre_calle' => 'Leon Gallo',
                'id_ciudad' => '34'
            ],
            [
                'nombre_calle' => 'Las Quilas',
                'id_ciudad' => '34'
            ],
            [
                'nombre_calle' => 'Imperial',
                'id_ciudad' => '34'
            ],
            [
                'nombre_calle' => 'Manquehue',
                'id_ciudad' => '35'
            ],
            [
                'nombre_calle' => 'Sarmiento',
                'id_ciudad' => '35'
            ],
            [
                'nombre_calle' => 'Baquedano',
                'id_ciudad' => '35'
            ],
            [
                'nombre_calle' => 'Fresia',
                'id_ciudad' => '36'
            ],
            [
                'nombre_calle' => 'Los Laureles',
                'id_ciudad' => '36'
            ],
            [
                'nombre_calle' => 'Los Avellanos',
                'id_ciudad' => '36'
            ],
            [
                'nombre_calle' => 'Los Boldos',
                'id_ciudad' => '37'
            ],
            [
                'nombre_calle' => 'Essen',
                'id_ciudad' => '37'
            ],
            [
                'nombre_calle' => 'Baden',
                'id_ciudad' => '37'
            ],
            [
                'nombre_calle' => 'Sotomayor',
                'id_ciudad' => '38'
            ],
            [
                'nombre_calle' => 'Nahuelco',
                'id_ciudad' => '38'
            ],
            [
                'nombre_calle' => 'Gamboa',
                'id_ciudad' => '38'
            ],
            [
                'nombre_calle' => 'Angamos',
                'id_ciudad' => '39'
            ],
            [
                'nombre_calle' => 'Gostoriaga',
                'id_ciudad' => '39'
            ],
            [
                'nombre_calle' => 'España',
                'id_ciudad' => '39'
            ],
        ]);
    }
}
