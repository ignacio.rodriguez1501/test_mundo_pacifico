<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provincias')->insert([
            [
                'nombre_provincia' => 'Concepcion',
                'id_region' => 1
            ],
            [
                'nombre_provincia' => 'Arauco',
                'id_region' => 1
            ],
            [
                'nombre_provincia' => 'Bio-Bio',
                'id_region' => 1
            ],
            [
                'nombre_provincia' => 'Chacabuco',
                'id_region' => 2
            ],
            [
                'nombre_provincia' => 'Cordillera',
                'id_region' => 2
            ],
            [
                'nombre_provincia' => 'Maipo',
                'id_region' => 2
            ],
            [
                'nombre_provincia' => 'Melipillla',
                'id_region' => 2
            ],
            [
                'nombre_provincia' => 'Santiago',
                'id_region' => 2
            ],
            [
                'nombre_provincia' => 'Talagante',
                'id_region' => 2
            ],
            [
                'nombre_provincia' => 'Cautin',
                'id_region' => 3
            ],
            [
                'nombre_provincia' => 'Malleco',
                'id_region' => 3
            ]
            ]);
    }
}
