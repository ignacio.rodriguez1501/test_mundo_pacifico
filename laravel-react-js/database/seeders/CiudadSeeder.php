<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ciudades')->insert([
            [
                'nombre_ciudad' => 'Chiguayante',
                'id_provincia' => 1
            ],
            [
                'nombre_ciudad' => 'Concepcion',
                'id_provincia' => 1
            ],
            [
                'nombre_ciudad' => 'Coronel',
                'id_provincia' => 1
            ],
            [
                'nombre_ciudad' => 'Hualpen',
                'id_provincia' => 1
            ],
            [
                'nombre_ciudad' => 'San Pedro de la Paz',
                'id_provincia' => 1
            ],
            [
                'nombre_ciudad' => 'Talcahuano',
                'id_provincia' => 1
            ],
            [
                'nombre_ciudad' => 'Cañete',
                'id_provincia' => 2
            ],
            [
                'nombre_ciudad' => 'Contulmo',
                'id_provincia' => 2
            ],
            [
                'nombre_ciudad' => 'Curanilahue',
                'id_provincia' => 2
            ],
            [
                'nombre_ciudad' => 'Lebu',
                'id_provincia' => 2
            ],
            [
                'nombre_ciudad' => 'Nacimiento',
                'id_provincia' => 3
            ],
            [
                'nombre_ciudad' => 'Mulchen',
                'id_provincia' => 3
            ],
            [
                'nombre_ciudad' => 'Yumbel',
                'id_provincia' => 3
            ],
            [
                'nombre_ciudad' => 'San Rosendo',
                'id_provincia' => 3
            ],
            [
                'nombre_ciudad' => 'Tucapel',
                'id_provincia' => 3
            ],
            [
                'nombre_ciudad' => 'Colina',
                'id_provincia' => 4
            ],
            [
                'nombre_ciudad' => 'Lampa',
                'id_provincia' => 4
            ],
            [
                'nombre_ciudad' => 'Tiltil',
                'id_provincia' => 4
            ],
            [
                'nombre_ciudad' => 'Puente Alto',
                'id_provincia' => 5
            ],
            [
                'nombre_ciudad' => 'San Jose de Maipo',
                'id_provincia' => 5
            ],
            [
                'nombre_ciudad' => 'Pirque',
                'id_provincia' => 5
            ],
            [
                'nombre_ciudad' => 'Buin',
                'id_provincia' => 6
            ],
            [
                'nombre_ciudad' => 'Paine',
                'id_provincia' => 6
            ],
            [
                'nombre_ciudad' => 'Calera de Tango',
                'id_provincia' => 6
            ],
            [
                'nombre_ciudad' => 'Maria Pinto',
                'id_provincia' => 7
            ],
            [
                'nombre_ciudad' => 'Curacavi',
                'id_provincia' => 7
            ],
            [
                'nombre_ciudad' => 'Alhue',
                'id_provincia' => 7
            ],
            [
                'nombre_ciudad' => 'La Cisterna',
                'id_provincia' => 8
            ],
            [
                'nombre_ciudad' => 'San Ramon',
                'id_provincia' => 8
            ],
            [
                'nombre_ciudad' => 'San Miguel',
                'id_provincia' => 8
            ],
            [
                'nombre_ciudad' => 'Padre Hurtado',
                'id_provincia' => 9
            ],
            [
                'nombre_ciudad' => 'Peñaflor',
                'id_provincia' => 9
            ],
            [
                'nombre_ciudad' => 'Talagante',
                'id_provincia' => 9
            ],
            [
                'nombre_ciudad' => 'Temuco',
                'id_provincia' => 10
            ],
            [
                'nombre_ciudad' => 'Padre las Casas',
                'id_provincia' => 10
            ],
            [
                'nombre_ciudad' => 'Galvarino',
                'id_provincia' => 10
            ],
            [
                'nombre_ciudad' => 'Angol',
                'id_provincia' => 11
            ],
            [
                'nombre_ciudad' => 'Puren',
                'id_provincia' => 11
            ],
            [
                'nombre_ciudad' => 'Victoria',
                'id_provincia' => 11
            ]
            ]);
    }
}
